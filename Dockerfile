FROM node:12-alpine

WORKDIR /srv

RUN apk add --no-cache git

RUN npm install -g jeromemacias/pino-raven

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run compile

CMD node lib/index.js | pino-raven --dsn="$SENTRY_DSN"