# General

Test project is built using typescript and koa.

For geocoding it uses google map geocoding api, fetched results are cached in Redis for quicker response next time

# API

There is single API endpoint ```/``` that accepts ```POST``` requests. Live version is available on ```https://blanket.tillers.lv/```.

Request body has to be:

```
{locations: string[]}
```

Response is:

```
{
  nearby: {[key: string]: string},
  badLocations: string[] //this is list of locations that could not be geocoded
}
```
# Example

Request:

```
{
  "locations": ["The Statue of Liberty", "Mount Rushmore", "Central Park", "Something bad"]
}
```

Response:

```
{
    "nearby": {
        "The Statue of Liberty": "Central Park",
        "Mount Rushmore": "Central Park",
        "Central Park": "The Statue of Liberty"
    },
    "badLocations": [
        "Something bad"
    ]
}
```

# Running

Project requires running Redis instance

All configuration is be done using environment variables(.env is supported)

* HTTP_PORT
* GOOGLE_API_KEY - google maps api key with access to geocoding serice
* PARALLEL_PROMISES - controls how many geocoding requests are executed in parallel
* REDIS_HOST
* REDIS_PORT
* LOG_LEVEL - controls pino output

To run just execute ```npm start```