import pLimit from "p-limit";

import geoCodingService from "./geocoding";

import {Coordinates} from "./geocoding";

import config from "../config";

// https://stackoverflow.com/a/21623206/1019431
function calculateDistance(loc1: Coordinates, loc2: Coordinates) {
  const p = 0.017453292519943295; // Math.PI / 180
  const c = Math.cos;
  const a =
    0.5 -
    c((loc2.lat - loc1.lat) * p) / 2 +
    (c(loc1.lat * p) * c(loc2.lat * p) * (1 - c((loc2.long - loc2.long) * p))) / 2;

  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

export interface DistanceResult {
  nearby: {[key: string]: string};
  badLocations: string[];
}

export default class DistanceService {
  static async getClosestPairs(locations: string[]): Promise<DistanceResult> {
    const lookup: {[key: string]: Coordinates} = {};

    const limit = pLimit(config.geoCoding.parallelPromises);

    const promises = locations.map((location) =>
      limit(() => geoCodingService.getCoordinates(location)),
    );

    const coordinates = await Promise.all(promises);
    const badLocations: string[] = [];

    // result of lookup will always be in same order as locations array
    for (let i = 0; i < locations.length; i++) {
      if (!coordinates[i]) {
        badLocations.push(locations[i]);
      } else {
        lookup[locations[i]] = coordinates[i];
      }
    }

    const nearbyResults = locations.reduce(
      (result, location, index) => {
        let min = Infinity;
        // it will be null if there are only one valid location, so a pair of locations
        // can't be formed
        let closest = null;

        // locations that could not be geocode will not have coordinates
        if (!lookup[location]) {
          return result;
        }

        for (let i = 0; i < locations.length; i++) {
          if (i === index) {
            continue;
          }

          if (!lookup[locations[i]]) {
            continue;
          }

          const distance = calculateDistance(lookup[location], lookup[locations[i]]);

          if (distance < min) {
            min = distance;
            closest = locations[i];
          }
        }

        result[location] = closest;

        return result;
      },
      {} as {[key: string]: string},
    );

    return {
      nearby: nearbyResults,
      badLocations,
    };
  }
}
