import {GoogleMapsClient} from "@google/maps";
import {RedisClient} from "redis";

import logger from "./logger";

export interface Coordinates {
  lat: number;
  long: number;
}

// for consistency when caching the results
const normalize = (input: string) => input.toLowerCase().trim();

// Redis is used as cache for subsequent requests as google api is noticeably slower
export class GeoCodingService {
  private googleClient: GoogleMapsClient;
  private redisClient: RedisClient;

  init(googleClient: GoogleMapsClient, redisClient: RedisClient) {
    this.googleClient = googleClient;
    this.redisClient = redisClient;
  }

  async getCoordinates(location: string): Promise<Coordinates> {
    location = normalize(location);

    let coordinates: Coordinates = await this.readFromCache(location);

    if (!!coordinates) {
      return coordinates;
    }

    coordinates = await this.readFromApi(location);

    if (!coordinates) {
      return;
    }

    await this.setCache(location, coordinates);

    return coordinates;
  }

  private async readFromCache(location: string): Promise<Coordinates> {
    return new Promise<Coordinates>((resolve, reject) =>
      this.redisClient.get("location:" + location, (err, value) => {
        if (err) {
          return reject(err);
        }

        if (!value) {
          return resolve();
        }

        return resolve(JSON.parse(value) as Coordinates);
      }),
    );
  }

  private async setCache(location: string, coordinates: Coordinates) {
    await new Promise<void>((resolve, reject) => {
      this.redisClient.set("location:" + location, JSON.stringify(coordinates), (error) => {
        if (error) {
          return reject(error);
        }

        return resolve();
      });
    });
  }

  private async readFromApi(location: string): Promise<Coordinates> {
    logger.debug("Will have to read location for %s", location);

    const result = await this.googleClient
      .geocode({
        address: location,
      })
      .asPromise();

    logger.debug("Data for location %s fetched", location);

    if (result.json.status === "ZERO_RESULTS") {
      logger.debug("Could not find place for location %s", location);
      return;
    }

    return {
      lat: result.json.results[0].geometry.location.lat,
      long: result.json.results[0].geometry.location.lng,
    };
  }
}

export default new GeoCodingService();
