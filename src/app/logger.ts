import Logger from "pino";
import config from "../config";

const logger = Logger({level: config.logging.level, enabled: process.env.NODE_ENV !== "test"});

export default logger;
