import createRouter from "koa-joi-router";

const router = createRouter();
const Joi = createRouter.Joi;

import DistanceService from "./distance";

router.post(
  "/",
  {
    validate: {
      body: {
        locations: Joi.array()
          .required()
          .items(
            Joi.string()
              .trim()
              .max(500)
              .min(2),
          )
          .max(100)
          .min(2),
      },
      type: "json",
    },
  },
  async (ctx) => {
    const locations = (ctx.request.body as {locations: string[]}).locations;

    const uniqueLocations = locations.filter(
      (location, index) =>
        locations.findIndex((value) => value.toLowerCase() === location.toLowerCase()) === index,
    );

    ctx.body = await DistanceService.getClosestPairs(uniqueLocations);
  },
);

export default router;
