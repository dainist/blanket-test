import Koa from "koa";
import bodyparser from "koa-bodyparser";
import logger from "koa-pino-logger";
import router from "./route";

const server = new Koa();

server.use(logger({enabled: process.env.NODE_ENV !== "test"}));

server.use(
  bodyparser({
    enableTypes: ["json"],
  }),
);

server.use(router.middleware());

export default server;
