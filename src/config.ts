import {config} from "dotenv";

config();

export default {
  httpPort: parseInt(process.env.HTTP_PORT, 10),
  geoCoding: {
    apiKey: process.env.GOOGLE_API_KEY,
    parallelPromises: parseInt(process.env.PARALLEL_PROMISES, 10) || 10,
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT, 10),
  },
  logging: {
    level: process.env.LOG_LEVEL || "info",
  },
};
