import config from "./config";
import server from "./app/server";
import logger from "./app/logger";

import {
  createClient as createGoogleClient,
} from "@google/maps";

import {
  createClient as createRedisClient,
} from "redis";

import GeoCodingService from "./app/geocoding";

const googleClient = createGoogleClient({
  key: config.geoCoding.apiKey,
  Promise,
});

const redisClient = createRedisClient({
  host: config.redis.host,
  port: config.redis.port,
});

GeoCodingService.init(googleClient, redisClient);

server.listen(config.httpPort);

logger.info("Server listening to port %d", config.httpPort);
