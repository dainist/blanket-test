import {expect} from "chai";
import distanceService from "../app/distance";
import geoCodingService from "../app/geocoding";
import {Coordinates} from "../app/geocoding";
import * as sinon from "sinon";

describe("Distance service", () => {
  afterEach(() => sinon.restore());

  describe("getClosestPairs", () => {
    it("should return closest location for each of the entries", async () => {
      sinon
        .stub(geoCodingService, "getCoordinates")
        .withArgs("location 1")
        .returns(
          Promise.resolve({
            lat: 1,
            long: 0,
          } as Coordinates),
        )
        .withArgs("location 2")
        .returns(
          Promise.resolve({
            lat: 4,
            long: 0,
          } as Coordinates),
        )
        .withArgs("location 3")
        .returns(
          Promise.resolve({
            lat: 5,
            long: 0,
          } as Coordinates),
        );

      const result = await distanceService.getClosestPairs([
        "location 1",
        "location 2",
        "location 3",
      ]);

      expect(result).to.be.deep.equal({
        badLocations: [],
        nearby: {
          "location 1": "location 2",
          "location 2": "location 3",
          "location 3": "location 2",
        },
      });
    });

    it("should filter out locations for whom coordinates can't be found in bad location array", async () => {
      sinon
        .stub(geoCodingService, "getCoordinates")
        .withArgs("location 1")
        .returns(
          Promise.resolve({
            lat: 1,
            long: 0,
          } as Coordinates),
        )
        .withArgs("location 2")
        .returns(
          Promise.resolve({
            lat: 4,
            long: 0,
          } as Coordinates),
        )
        .withArgs("location 3")
        .returns(Promise.resolve(undefined));

      const result = await distanceService.getClosestPairs([
        "location 1",
        "location 2",
        "location 3",
      ]);

      expect(result).to.be.deep.equal({
        badLocations: ["location 3"],
        nearby: {
          "location 1": "location 2",
          "location 2": "location 1",
        },
      });
    });

    it("should return null for closest location if second location isn't correct", async () => {
      sinon
        .stub(geoCodingService, "getCoordinates")
        .withArgs("location 1")
        .returns(
          Promise.resolve({
            lat: 1,
            long: 0,
          } as Coordinates),
        )
        .withArgs("location 2")
        .returns(Promise.resolve(undefined));

      const result = await distanceService.getClosestPairs(["location 1", "location 2"]);

      expect(result).to.be.deep.equal({
        badLocations: ["location 2"],
        nearby: {
          "location 1": null,
        },
      });
    });
  });
});
