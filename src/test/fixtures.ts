/* tslint:disable:max-line-length */

export const goodResponse: any = {
  status: 200,
  headers: {
    "content-type": "application/json; charset=UTF-8",
    "date": "Mon, 08 Jul 2019 18:46:41 GMT",
    "expires": "Mon, 08 Jul 2019 18:51:41 GMT",
    "cache-control": "public, max-age=300",
    "server": "scaffolding on HTTPServer2",
    "x-xss-protection": "0",
    "x-frame-options": "SAMEORIGIN",
    "server-timing": "gfet4t7; dur=558",
    "alt-svc": "quic=\":443\"; ma=2592000; v=\"46,43,39\"",
    "accept-ranges": "none",
    "vary": "Accept-Language,Accept-Encoding",
    "connection": "close",
  },
  json: {
    html_attributions: [],
    results: [
      {
        formatted_address: "New York, NY 10004, USA",
        geometry: {
          location: {
            lat: 40.6892494,
            lng: -74.04450039999999,
          },
          viewport: {
            northeast: {
              lat: 40.71814749999999,
              lng: -73.99872490000001,
            },
            southwest: {
              lat: 40.6796167,
              lng: -74.05975889999998,
            },
          },
        },
        icon: "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
        id: "5a0d7e67078e35af0c456a277df9ffba7c1e4da6",
        name: "Statue of Liberty National Monument",
        opening_hours: {
          open_now: true,
        },
        photos: [
          {
            height: 5312,
            html_attributions: [
              "<a href=\"https://maps.google.com/maps/contrib/118296288152495182017/photos\">miki nogal</a>",
            ],
            photo_reference:
              "CmRaAAAAK7Qyhl5z1Nzkf2pVsZF3zE2PkluCAuNe2Mwc2W_kokFvAWPp_xLFNpDTYhKaqbV4Pe70R6f2mKCUeBwOHZOdHTYKsmi6GIvDaYlBMSHLjPwjTQHFhY13vLi-XSMH7JyfEhDB5xlhfIneNCzpxlXWXp9uGhSXWPWGHQhL01QPTUA40mJhqee-eg",
            width: 2988,
          },
        ],
        place_id: "ChIJPTacEpBQwokRKwIlDXelxkA",
        plus_code: {
          compound_code: "MXQ4+M5 New York, USA",
          global_code: "87G7MXQ4+M5",
        },
        rating: 4.6,
        reference: "ChIJPTacEpBQwokRKwIlDXelxkA",
        types: ["park", "point_of_interest", "establishment"],
        user_ratings_total: 53182,
      },
    ],
    status: "OK",
  },
  requestUrl:
    "https://maps.googleapis.com/maps/api/place/textsearch/json?query=the%20statue%20of%20liberty&key=AIzaSyCgWihtKj-kB6S7KvPguazUZK-kCZSVA7g",
  query: {
    query: "the statue of liberty",
    key: "AIzaSyCgWihtKj-kB6S7KvPguazUZK-kCZSVA7g",
  },
};

export const emptyResponse: any = {
  status: 200,
  headers: {
    "content-type": "application/json; charset=UTF-8",
    "date": "Mon, 08 Jul 2019 18:52:06 GMT",
    "expires": "Mon, 08 Jul 2019 18:57:06 GMT",
    "cache-control": "public, max-age=300",
    "server": "scaffolding on HTTPServer2",
    "x-xss-protection": "0",
    "x-frame-options": "SAMEORIGIN",
    "server-timing": "gfet4t7; dur=356",
    "alt-svc": "quic=\":443\"; ma=2592000; v=\"46,43,39\"",
    "accept-ranges": "none",
    "vary": "Accept-Language,Accept-Encoding",
    "connection": "close",
  },
  json: {
    html_attributions: [],
    results: [],
    status: "ZERO_RESULTS",
  },
  requestUrl:
    "https://maps.googleapis.com/maps/api/place/textsearch/json?query=burrr%20burr&key=AIzaSyCgWihtKj-kB6S7KvPguazUZK-kCZSVA7g",
  query: {
    query: "burrr burr",
    key: "AIzaSyCgWihtKj-kB6S7KvPguazUZK-kCZSVA7g",
  },
};
