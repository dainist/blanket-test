import {expect} from "chai";
import * as sinon from "sinon";

import {createClient as createGoogleClient, GoogleMapsClient} from "@google/maps";

import {RedisClient} from "redis";

import {Coordinates} from "../app/geocoding";

import {goodResponse, emptyResponse} from "./fixtures";

import geoCodingService from "../app/geocoding";

describe("Geocoding service", () => {
  let redis: RedisClient;
  let google: GoogleMapsClient;

  beforeEach(() => {
    // tslint:disable-next-line:no-empty
    redis = {get: () => {}, set: () => {}} as any;

    google = createGoogleClient({
      key: "something",
      Promise,
    });
    geoCodingService.init(google, redis);
  });

  afterEach(() => sinon.restore());

  describe("getCoordinates", () => {
    it("should be able to fetch coordinates from redis cache", async () => {
      sinon
        .stub(redis, "get")
        .withArgs("location:some location", sinon.match.func)
        .yields(null, JSON.stringify({lat: 1, long: 13} as Coordinates));

      expect(await geoCodingService.getCoordinates("   Some locatioN   ")).to.be.deep.equal({
        lat: 1,
        long: 13,
      });
    });

    it("should be able to fetch and cache coordinates from google api", async () => {
      const setStub = sinon
        .stub(redis as any, "set")
        .withArgs(
          "location:some location",
          JSON.stringify({lat: 40.6892494, long: -74.04450039999999}),
          sinon.match.func,
        )
        .yields(null, null);

      sinon
        .stub(redis, "get")
        .withArgs("location:some location", sinon.match.func)
        .yields();

      sinon
        .stub(google, "places")
        .withArgs({
          query: "some location",
        })
        .returns({asPromise: () => Promise.resolve(goodResponse)} as any);

      expect(await geoCodingService.getCoordinates("   Some locatioN   ")).to.be.deep.equal({
        lat: 40.6892494,
        long: -74.04450039999999,
      });

      expect(setStub.called).to.be.equal(true);
    });

    it("should return undefined if google api can't find coordinates for given location", async () => {
      const setStub = sinon.stub(redis, "set").yields();

      sinon
        .stub(redis, "get")
        .withArgs("location:some location", sinon.match.func)
        .yields();

      sinon
        .stub(google, "places")
        .withArgs({
          query: "some location",
        })
        .returns({asPromise: () => Promise.resolve(emptyResponse)} as any);

      expect(await geoCodingService.getCoordinates("   Some locatioN   ")).to.be.equal(undefined);

      expect(setStub.called).to.be.equal(false);
    });
  });
});
