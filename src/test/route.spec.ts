import server from "../app/server";
import supertest from "supertest";
import {Server} from "http";
import * as sinon from "sinon";
import DistanceService from "../app/distance";
import {DistanceResult} from "../app/distance";

describe("api endpoint", () => {
  let instance: Server;
  beforeEach((done) => {
    instance = server.listen(done);
  });

  afterEach((done) => {
    sinon.restore();
    instance.close(done);
  });

  it("should fetch location distances", async () => {
    sinon
      .stub(DistanceService, "getClosestPairs")
      .withArgs(["some location 1", "not good", "some location 2", "some location 3"])
      .returns(
        Promise.resolve({
          badLocations: ["not good"],
          nearby: {
            "some location 1": "some location 2",
            "some location 2": "some location 3",
            "some location 3": "some location 2",
          },
        } as DistanceResult),
      );

    await supertest(instance)
      .post("/")
      .send({
        locations: [
          "        some location 1      ",
          "not good",
          "some location 2",
          "some location 3",
        ],
      })
      .expect(200, {
        badLocations: ["not good"],
        nearby: {
          "some location 1": "some location 2",
          "some location 2": "some location 3",
          "some location 3": "some location 2",
        },
      });
  });

  it("should filter out duplicates", async () => {
    sinon
      .stub(DistanceService, "getClosestPairs")
      .withArgs(["some location 1", "some location 2"])
      .returns(
        Promise.resolve({
          badLocations: [],
          nearby: {
            "some location 1": "some location 2",
            "some location 2": "some location 1",
          },
        } as DistanceResult),
      );

    await supertest(instance)
      .post("/")
      .send({
        locations: [
          "        some location 1      ",
          "some location 2",
          "some location 1",
          "Some locatioN 2",
        ],
      })
      .expect(200, {
        badLocations: [],
        nearby: {
          "some location 1": "some location 2",
          "some location 2": "some location 1",
        },
      });
  });

  it("should reject requests that are with more than 100 locations", async () => {
    await supertest(instance)
      .post("/")
      .send({
        locations: new Array(101).fill("Some location"),
      })
      .expect(400, {});
  });

  it("should reject requests that are with only 1 location", async () => {
    await supertest(instance)
      .post("/")
      .send({
        locations: ["test"],
      })
      .expect(400, {});
  });

  it("should reject requests that don't have any locations set", async () => {
    await supertest(instance)
      .post("/")
      .send({})
      .expect(400, {});
  });

  it("should reject requests if one of locations is longer than 500 chars", async () => {
    await supertest(instance)
      .post("/")
      .send({
        locations: ["some location", new Array(502).join("a")],
      })
      .expect(400, {});
  });

  it("should reject requests if one of locations isn't even 2 chars long", async () => {
    await supertest(instance)
      .post("/")
      .send({
        locations: ["some location", "a"],
      })
      .expect(400, {});
  });
});
